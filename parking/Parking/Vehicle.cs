﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    public class Vehicle
    {
        double size;
        bool isTurnedOn = false;
        public Vehicle(double size)
        {
            this.size = size;
        }
        public double Size
        {
            get
            {
                return size;
            }
        }
        public void TurnOn()
        {
            isTurnedOn = true;
        }
        public void TurnOff()
        {
            isTurnedOn = false;
        }
    }
    public class Motorcycle : Vehicle
    {
        string brand;
        public Motorcycle(double size, string brand) : base(size)
        {
            this.brand = brand;
        }
        public string Brand
        {
            get
            {
                return brand;
            }
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
    public class Car : Vehicle
    {
        string color;
        public Car(string color, double size) : base(size)
        {
            this.color = color;
        }
        public string Color
        {
            get
            {
                return color;
            }
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
    public class Truck : Vehicle
    {
        public Truck(double size) : base(size)
        {

        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
