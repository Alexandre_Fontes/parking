﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    public class ParkingLot
    {
        double capacity;
        double vacancy;
        List<Valet> valets = new List<Valet>();
        List<Vehicle> parkedVehicles = new List<Vehicle>();
        public ParkingLot(double capacity)
        {
            this.capacity = capacity;
            vacancy = capacity;
        }

        public double Vacancy
        {
            get
            {
                return vacancy;
            }
        }

        public void Park(Vehicle vehicle)
        {
            if (vacancy > 0)
            {
                if (vehicle.Size <= Vacancy)
                {
                    if (!parkedVehicles.Contains(vehicle))
                    {
                        Valet valet = ChooseValet();
                        valet.Drive(vehicle);
                        parkedVehicles.Add(vehicle);
                        Console.WriteLine("the vehicle has been parked");
                        vacancy = vacancy - vehicle.Size;
                        valet.Available = true;

                    }
                    else
                    {
                        throw new VehiculeAlreadyParkedException();
                    }
                }
                else
                {
                    throw new ParkingLotInsufficient_Expection();
                }
            }
            else
            {
                throw new ParkingLotFull_Expection();
            }
        }

        public void vacate(Vehicle vehicle)
        {

        }

        public void Hire(Valet valet)
        {
            valets.Add(valet);
        }

        public void Fire(Valet valet)
        {
            valets.Remove(valet);
        }

        private Valet ChooseValet()
        {
            foreach (Valet valet in valets)
            {
                if (valet.Available)
                {
                    return valet;
                }
            }
            throw new NotAvailableValets();
        }
        private bool IsVehicleParked(Vehicle vehicle)
        {
            return true;
        }

        private bool IsParkFull()
        {
            return true;
        }

        private void PutVehicleInPark(Vehicle vehicle)
        {
            
        }

        private void CheckVacancy(double size)
        {

        }
    }

    public class ParkingLotInsufficient_Expection : Exception
    {

    }

    public class VehiculeAlreadyParkedException : Exception
    {

    }

    public class NotAvailableValets : Exception
    {

    }

    public class ParkingLotFull_Expection : Exception
    {

    }
}
