﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parking
{
    public class Valet
    {
        string name;
        bool available = true;
        public Valet(string name)
        {
            this.name = name;
        }
        public bool Available
        {
            get
            {
                return available;
            }
            set
            {
                available = value;
            }
        }
        public void Drive(Vehicle vehicle)
        {
            available = false;
            vehicle.TurnOn();
            vehicle.TurnOff();
        }
    }
}
