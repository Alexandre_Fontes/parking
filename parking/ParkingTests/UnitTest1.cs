﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Parking
{
    [TestClass]
    public class PreExamTest
    {
        int capacity;
        private ParkingLot parkingLot;
        private Vehicle vehicle;
        private Motorcycle motocycle;

        /// <summary>
        /// This test method checks after the first car parked, the parking vacancy is up to date
        /// Sequence diagram 1.0
        /// </summary>
        [TestMethod]
        public void Park_SequenceDiagrammeRegularPark_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.vehicle = new Vehicle(10);
            double vacancyExpected = 90;
            double vacancyActual = 0;

            //when
            parkingLot.Park(this.vehicle);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
        /// <summary>
        /// This test method checks if the vehicle size accessor gets the correct value expected
        /// Sequence diagram 1.1
        /// </summary>
        [TestMethod]
        public void Size_Accessor_Success()
        {
            //given
            double expectedSize = 10;
            Vehicle vehicle = new Vehicle(expectedSize);
            double actualSize = 0;

            //when
            actualSize = vehicle.Size;

            //then
            Assert.AreEqual(expectedSize, actualSize);
        }
        [TestMethod]
        public void Drive_ValetAvailable_Success()
        {
            //given
            Vehicle vehicle = new Vehicle(10);
            Valet valet = new Valet("Bob");
            //a valet is by default available just after instanciation
            bool actualAvailability = true;

            //when
            valet.Drive(vehicle);
            actualAvailability = valet.Available;

            //then
            Assert.IsFalse(valet.Available);
        }

        /// <summary>
        /// This test method checks for park a truck
        /// Sequence diagram
        /// </summary>
        [TestMethod]
        public void Park_Truck_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            Truck truck = new Truck(40);
            double vacancyExpected = 60;
            double vacancyActual = 0;

            //when
            parkingLot.Park(truck);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
        /// <summary>
        /// This test method checks if the valet availability is true when Valet is not driving
        /// </summary>
        [TestMethod]
        public void Park_ValetAvailable_Success()
        {
            //given
            ParkingLot parkingLot = new ParkingLot(500);
            Vehicle vehicle = new Vehicle(10);
            Valet valet = new Valet("Bob");
            //a valet is by default available just after instanciation
            bool actualAvailability = true;

            //when
            parkingLot.Hire(valet);
            parkingLot.Park(vehicle);
            actualAvailability = valet.Available;

            //then
            Assert.IsTrue(valet.Available);
        }
        /// <summary>
        /// This test tests if a vehicule is already parked and throw an expeception
        ///</summary>
        [TestMethod]
        [ExpectedException(typeof(VehiculeAlreadyParkedException))]
        public void Park_VehicleAlreadyParked_Exception()
        {
            //given
            this.vehicle = new Vehicle(10);
            this.parkingLot = new ParkingLot(400);
            this.parkingLot.Hire(new Valet("Mishiali"));
            this.parkingLot.Park(vehicle);
            //when
            this.parkingLot.Park(vehicle);
            //then

        }
        /// This test method checks if the parkinglot is full
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ParkingLotFull_Expection))]
        public void Park_FullParkingLot_Exception()
        {

            //GIVEN
            this.capacity = 0;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));

            this.vehicle = new Vehicle(10);
            //WHEN
            this.parkingLot.Park(vehicle);
        }
        /// <summary>
        ///  This test method checks if the parkinglot has enough space for the current vehicle that we want to park
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ParkingLotInsufficient_Expection))]
        public void Park_InsufficentSpaceForVehicle_Exception()
        {
            //GIVEN
            this.capacity = 10;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));
            this.vehicle = new Vehicle(11);
            //WHEN
            this.parkingLot.Park(vehicle);
        }
        /// <summary>
        /// This test method checks after the first motorcycle parked, the parking vacancy is up to date
        /// Sequence diagram 1.0
        /// </summary>
        [TestMethod]
        public void Park_ParMotorCycle_Success()
        {
            //given
            this.capacity = 100;
            this.parkingLot = new ParkingLot(capacity);
            this.parkingLot.Hire(new Valet("Bob"));

            this.motocycle = new Motorcycle(5, "Harley");
            double vacancyExpected = 95;
            double vacancyActual = 0;

            //when
            parkingLot.Park(this.motocycle);
            vacancyActual = parkingLot.Vacancy;

            //then
            Assert.AreEqual(vacancyExpected, vacancyActual);
        }
    }
}
