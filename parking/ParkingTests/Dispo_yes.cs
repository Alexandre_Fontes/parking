﻿namespace Parking
{
    internal class Dispo_yes
    {
        private string name;
        private bool available;

        public Dispo_yes(string name, bool available)
        {
            this.name = name;
            this.available = available;
        }
    }
}